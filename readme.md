# Twitch Streamteam Lister

## Prerequisites
* Python 3.6
* pip(or pip3) install -r requirements.txt
* The program itself (Go into the Repository view through the top menu bar; the download button is in the upper right)... or just clone this repo with git

## How to use
Go into the settings, insert teams and individual users. You're then ready to go.
## Screenshot
![Screenshot 1](https://i.imgur.com/FA3hagv.png)