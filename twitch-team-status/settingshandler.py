import operator

class SettingsHandler():
	def __init__(self, dbHandler):
		self.dbHandler = dbHandler
		self.settingsDictionary = {}
		self.updateSettingsDictionary()

	def updateSettingsDictionary(self):
		self.settingsDictionary = self.dbHandler.loadSettings()

	def getSettingsTeams(self):
		self.updateSettingsDictionary()
		teams = []
		for team in self.settingsDictionary["streamteams"]:
			teams.append(ConfigStreamTeam(team[0], team[1]))

		teams.sort(key=operator.attrgetter("teamname"))
		return teams

	def getSettingsStreamers(self):
		streamers = []
		for streamer in self.settingsDictionary["streamers"]:
			streamers.append(ConfigStreamer(streamer[0]))

		return streamers

class ConfigStreamTeam:
	def __init__(self, teamurl, teamname):
		self.teamurl = teamurl
		self.teamname = teamname

class ConfigStreamer:
	def __init__(self, streamername):
		self.streamername = streamername