from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class SettingsWindow(QWidget):
	def __init__(self, dbHandler, settingsHandler, mainWindow):
		super().__init__()

		# Blocks any input to all other windows
		self.setWindowModality(Qt.ApplicationModal)

		self.setWindowTitle("Settings")

		self.dbHandler = dbHandler
		self.settingsHandler = settingsHandler
		self.mainWindow = mainWindow

		self.initUI()

	def initUI(self):

		mainGrid = QGridLayout()

		buttonSizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

		self.okButton = QPushButton("OK")
		self.okButton.setCursor(Qt.PointingHandCursor)
		self.okButton.setSizePolicy(buttonSizePolicy)
		self.okButton.clicked.connect(lambda: self.okButtonAction())

		self.cancelButton = QPushButton("Cancel")
		self.cancelButton.setCursor(Qt.PointingHandCursor)
		self.cancelButton.setSizePolicy(buttonSizePolicy)
		self.cancelButton.clicked.connect(lambda: self.cancelButtonAction())

		self.applyButton = QPushButton("Apply")
		self.applyButton.setCursor(Qt.PointingHandCursor)
		self.applyButton.setSizePolicy(buttonSizePolicy)
		self.applyButton.clicked.connect(lambda: self.applyButtonAction())

		buttonpane = QWidget()
		buttonpaneL = QHBoxLayout()
		buttonpaneL.setAlignment(Qt.AlignCenter)

		buttonpaneL.addWidget(self.okButton)
		buttonpaneL.addWidget(self.cancelButton)
		buttonpaneL.addWidget(self.applyButton)

		buttonpane.setLayout(buttonpaneL)

		self.setLayout(mainGrid)

		self.teamTextEdit = QTextEdit()
		self.teamTextEdit.setPlaceholderText("majorragers:Major Ragers\nsherpasquad:The Sherpa Squad")
		self.teamTextEdit.setText(self.assembleTeamsTextEditText(self.settingsHandler.settingsDictionary))
		self.streamersTextEdit = QTextEdit()
		self.streamersTextEdit.setPlaceholderText("strongdoctor\nshackleshotgun")
		self.streamersTextEdit.setText(self.assembleStreamersTextEditText(self.settingsHandler.settingsDictionary))

		# The text areas for streamers and teams
		mainGrid.addWidget(QLabel("Teams:"), 0, 0, 1, 5, Qt.AlignLeft)
		mainGrid.addWidget(self.teamTextEdit, 1, 0, 5, 5)
		mainGrid.addWidget(QLabel("Streamers:"), 6, 0, 1, 5, Qt.AlignLeft)
		mainGrid.addWidget(self.streamersTextEdit, 7, 0, 5, 5)
		mainGrid.addWidget(buttonpane, 12, 0, 1, 5)

	def saveSettings(self):
		settingsDictionary = {}
		settingsDictionary['streamteams'] = []
		settingsDictionary['streamers'] = []
		

		streamersText = self.streamersTextEdit.toPlainText()
		teamText = self.teamTextEdit.toPlainText()

		for team in teamText.splitlines():
			try:
				splitTeam = team.split(':')
				teamName = splitTeam[0].strip()
				teamDisplayName = splitTeam[1].strip()
				settingsDictionary['streamteams'].append((teamName, teamDisplayName))
			except IndexError:
				
				raise

		for streamer in streamersText.splitlines():
			streamer = streamer.strip()

			# Needs to be tuple for sqlite's executemany function
			settingsDictionary['streamers'].append((streamer,))

		self.dbHandler.saveSettings(settingsDictionary)
		self.mainWindow.updateTabs()
		self.mainWindow.tabs.repaint()

	def closeSettingsWindow(self):
		self.close()

	def okButtonAction(self):
		try:
			self.saveSettings()
			self.closeSettingsWindow()
			self.mainWindow.refreshAllButtonAction()
		except IndexError:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Warning)
			msg.setText("Invalid Teams field format. Please use \"teamlinkname:Team Display Name\"")
			msg.setWindowTitle("Invalid Format")
			msg.exec()

	def cancelButtonAction(self):
		self.closeSettingsWindow()
		self.mainWindow.refreshAllButtonAction()

	def applyButtonAction(self):
		try:
			self.saveSettings()
		except IndexError:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Warning)
			msg.setText("Invalid Teams field format. Please use \"teamlinkname:Team Display Name\"")
			msg.setWindowTitle("Invalid Format")
			msg.exec()

	def assembleTeamsTextEditText(self, settingsDict):
		stringList = []
		for teamTuple in settingsDict['streamteams']:
			stringList.append(teamTuple[0])
			stringList.append(":")
			stringList.append(teamTuple[1])
			stringList.append("\n")
		return ''.join(stringList)

	def assembleStreamersTextEditText(self, settingsDict):
		stringList = []
		for teamTuple in settingsDict['streamers']:
			stringList.append(teamTuple[0])
			stringList.append("\n")
		return ''.join(stringList)