class StreamTeam:
    def __init__(self, teamname, streamers):
    	assert type(teamname) is str
    	assert type(streamers) is dict

    	self.teamname = teamname
    	self.streamers = streamers