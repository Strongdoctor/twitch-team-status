import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os", "urllib3", "queue", "sqlite3", "abc"], "excludes": ["tkinter"], "optimize": 1}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "Twitch Team Status",
		version = "0.2",
		description = "Twitch Team Status",
		options = {"build_exe": build_exe_options},
		executables = [Executable(
			"twitch-team-status.pyw",
			base=base
		)]
)