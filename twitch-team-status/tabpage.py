from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class TabPage(QWidget):
	def __init__(self, teamName, teamDisplayName):
		self.teamName = teamName
		self.teamDisplayName = teamDisplayName
		super(TabPage, self).__init__()
		self.initUI()

	def initUI(self):
		self.tableWidget = QTableWidget()
		self.tableWidget.setRowCount(1)
		self.tableWidget.setColumnCount(7)
		self.tableWidget.setHorizontalHeaderLabels(("Name","Status","Game","Viewers","Hosted", "Visited", "Raided"))
		self.tableWidget.verticalHeader().setVisible(False)
		self.tableWidget.setFrameStyle(QFrame.NoFrame)
		self.tableWidget.setShowGrid(False)
		self.reinitTableCols()

		grid = QGridLayout()
		grid.addWidget(self.tableWidget)

		self.setLayout(grid)
		self.show()

	def reinitTableCols(self):
		#Make Name and Game columns stretch or whatever
		headers = self.tableWidget.horizontalHeader()
		headers.setSectionResizeMode(0, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(1, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(2, QHeaderView.Stretch)
		headers.setSectionResizeMode(3, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(4, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(5, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(6, QHeaderView.ResizeToContents)
		headers.setSectionResizeMode(7, QHeaderView.ResizeToContents)