/* GENERAL */
* {
	font-family: "SourceSansPro-Regular";
}

QMainWindow, QStatusBar, QMenuBar, QMenu, QDialog *, QTextEdit, QLabel {
	color: #f6f6ee;
}

QMainWindow, QDialog, QWidget {
	background: #161616;
}

/* MENU BAR */
QMenu, QMenuBar {
	background: #4b4b4b;
}

QMenu {
	border: 1px solid #272727;
}

QMenuBar::item, QMenu::item {
	padding: 6px 12px;
}

QMenuBar::item:selected, QMenu::item:selected {
	background: #272727;
	border: none;
}

/* STATUS BAR */
QStatusBar {
	background: #4b4b4b;
	margin: 0;
	padding-top: 10px;
}

/* TABLE */
QTableView {
	color: #f6f6f6;
	background: #272727;
	outline: none;
}

QHeaderView::section:horizontal {
	background: #272727;
	padding: 5px;
	color: #f6f6f6;
	border-style: none;
	border-right: 1px solid #161616;
	border-bottom: 1px solid #161616;
}

QTableWidget::item {
	border-bottom: 1px solid #202020;
	border-right: 1px solid #202020;
}

QTableWidget::item:last {
	border-bottom: none;
	border-right: none;
}

QTableWidget::item:selected {
	background: #4b4b4b;
	outline: none;
}

QTableWidget::item:focus {
	outline: none;
	border: none;
}

QTabWidget {
	background: #4b4b4b;
	outline: none;
}

QTabWidget::pane {
	padding: -9px;
	margin: 0;
}


/* TAB BAR */
QTabBar::tab {
	background: qlineargradient(x1:0 y1:0, x2:0 y2:1, stop:0 #494949, stop:0.4 #444444, stop:0.9 #444444, stop:1 #333333);
	color: #c2c2c2;
	border-top: 1px solid #676767;
	border-right: 1px solid #676767;
	padding: 8px 15px;
	top: 2px;
}

QTabBar::tab:first {
	border-top-left-radius: 5px;

}

QTabBar::tab:last {
	border-top-right-radius: 5px;
	border-right: none;
}

QTabBar::tab:selected {
	background: qlineargradient(x1:0 y1:0, x2:0 y2:1, stop:0 #333333, stop:0.4 #282828, stop:0.5 #282828, stop:1 #282828);
	color: #f6f6f6;
}

/* BUTTONS */
QPushButton {
	border-radius: 5px;
	background: qlineargradient(x1:0 y1:0, x2:0 y2:1, stop:0 #4b4b4b, stop:1 #282828);
	padding: 6px 10px;
	color: #f6f6f6;
	border: 1px solid #4b4b4b;
}

QPushButton:!enabled {
	background: #282828;
	color: #c2c2c2;
}

QPushButton:hover {
	border-color: #676767;
}

/* CHECKBOXES */
QCheckBox {
	color: #f6f6ee;
}

/* SCROLLBARS */
QScrollBar:vertical {
    background: #333333;
    margin: 0;
}

QScrollBar::handle:vertical {
    width: 10px;
    border: 1px outset #333333;
}

QScrollBar::sub-page:vertical, QScrollBar::add-page:vertical {
	background: #161616;
}

/* Remove the up and down buttons */
QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
    width: 0;
    height: 0;
    padding: 0;
}

/* Text Editing Boxes */
QTextEdit:focus {
	border: 1px solid #7154a5;
}