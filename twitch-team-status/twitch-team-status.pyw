from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon, QFontDatabase
import sys
import os
import mainwindow
import ctypes
import datetime
import logging

DIR_PATH = os.path.abspath(os.path.dirname(sys.argv[0]))

FORMAT = "%(asctime)s - %(name)s - %(levelname)s: %(message)s"
logging.basicConfig(format=FORMAT, filename=DIR_PATH + os.sep + "logs" + os.sep + 'errors.log',level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(handler)

def startApplication():
	
	# Windows is picky sometimes
	myappid = u'strongdoctor.twitch-team-status.gui-app.0.2' # arbitrary string
	ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

	app = QApplication(sys.argv)

	# Load custom font
	QFontDatabase.addApplicationFont(DIR_PATH + os.sep + "resources" + os.sep + "SourceSansPro-Regular.ttf")

	# Load CSS
	qssFile = DIR_PATH + os.sep + "resources" + os.sep + "styles.qss"
	with open(qssFile,"r") as fh:
		app.setStyleSheet(fh.read())

	# Set the app's default icon
	app_icon = QIcon()
	app_icon.addFile(DIR_PATH + os.sep + "resources" + os.sep + "tts-logo.png", QSize(256,256))
	app.setWindowIcon(app_icon)

	ex = mainwindow.MainWindow()
	sys.exit(app.exec_())

def handle_exception(exc_type, exc_value, exc_traceback):
	if issubclass(exc_type, KeyboardInterrupt):
		sys.__excepthook__(exc_type, exc_value, exc_traceback)
		return

	logger.error("Uncaught exception:", exc_info=(exc_type, exc_value, exc_traceback))

if __name__ == '__main__':

	# Set up logging
	sys.excepthook = handle_exception
	FORMAT = "%(asctime)s - %(name)s - %(levelname)s: %(message)s"
	logging.basicConfig(format=FORMAT, filename=DIR_PATH + os.sep + "logs" + os.sep + 'errors.log',level=logging.ERROR, datefmt='%Y-%m-%d %H:%M:%S')

	startApplication()