import sqlite3
import os
import sys

class DBHandler:
	def __init__(self, DBFilePath):
		self.DBFilePath = DBFilePath
		self.initializeDB()

	def saveStreamerCheckboxes(self, checkedBoxes):
		assert type(checkedBoxes) == dict
		print(checkedBoxes)
		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()

		c.execute('''
			CREATE UNIQUE INDEX IF NOT EXISTS idx_positions_title ON checkedhosted (streamername)
			''')

		c.execute('''
			CREATE UNIQUE INDEX IF NOT EXISTS idx_positions_title ON checkedvisited (streamername)
			''')

		c.execute('''
			CREATE UNIQUE INDEX IF NOT EXISTS idx_positions_title ON checkedraided (streamername)
			''')

		checkedList = []
		visitedList = []
		raidedList = []

		for streamerName, dateOfCheck in checkedBoxes['hosted'].items():
			checkedList.append((streamerName, dateOfCheck)) 

		for streamerName, dateOfCheck in checkedBoxes['visited'].items():
			visitedList.append((streamerName, dateOfCheck))

		for streamerName, dateOfCheck in checkedBoxes['raided'].items():
			raidedList.append((streamerName, dateOfCheck)) 

		c.executemany('''
				INSERT OR REPLACE INTO checkedhosted (streamername, date)
				VALUES (?, ?)
				''', checkedList)

		c.executemany('''
				INSERT OR REPLACE INTO checkedvisited (streamername, date)
				VALUES (?, ?)
				''', visitedList)

		c.executemany('''
				INSERT OR REPLACE INTO checkedraided (streamername, date)
				VALUES (?, ?)
				''', raidedList)

		conn.commit()
		conn.close()

	def fetchStreamerCheckboxes(self):
		checkedBoxes = {
			'hosted': dict(),
			'visited': dict(),
			'raided': dict(),
		}
		

		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()

		listOfCheckedRowsToDel = []
		for row in c.execute('SELECT * FROM checkedhosted'):
			listOfCheckedRowsToDel.append(row)

		listOfVisitedRowsToDel = []
		for row in c.execute('SELECT * FROM checkedvisited'):
			listOfVisitedRowsToDel.append(row)

		listOfRaidedRowsToDel = []
		for row in c.execute('SELECT * FROM checkedraided'):
			listOfRaidedRowsToDel.append(row)

		for row in listOfCheckedRowsToDel:
			if row[1] != None:
				checkedBoxes['hosted'][row[0]] = row[1]

		for row in listOfVisitedRowsToDel:
			if row[1] != None:
				checkedBoxes['visited'][row[0]] = row[1]

		for row in listOfRaidedRowsToDel:
			if row[1] != None:
				checkedBoxes['raided'][row[0]] = row[1]

		conn.commit()
		conn.close()

		return checkedBoxes

	def initializeDB(self):
		directory = os.path.abspath(os.path.dirname(sys.argv[0]))

		if not os.path.exists(directory + os.sep + "db"):
			os.makedirs(directory + os.sep + "db")

		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()

		c.execute('''
			CREATE TABLE IF NOT EXISTS checkedhosted
			(streamername text PRIMARY KEY, date text)
			''')

		c.execute('''
			CREATE TABLE IF NOT EXISTS checkedvisited
			(streamername text PRIMARY KEY, date text)
			''')

		c.execute('''
			CREATE TABLE IF NOT EXISTS checkedraided
			(streamername text PRIMARY KEY, date text)
			''')

		c.execute('''
			CREATE TABLE IF NOT EXISTS settingsteams
			(teamname text PRIMARY KEY, teamdisplayname text)
			''')

		c.execute('''
			CREATE TABLE IF NOT EXISTS settingsstreamers
			(streamername text PRIMARY KEY)
			''')

		conn.commit()
		conn.close()

	def deleteExtraRows(self):
		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()
		
		c.execute('DELETE FROM checkedhosted WHERE date IS NULL')
		c.execute('DELETE FROM checkedvisited WHERE date IS NULL')
		c.execute('DELETE FROM checkedraided WHERE date IS NULL')

		conn.commit()
		conn.close()

	def saveSettings(self, settingsDictionary):
		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()

		c.execute('DELETE FROM settingsstreamers') # Delete all rows in table
		c.execute('DELETE FROM settingsteams') # Delete all rows in table

		# Populate table with streamers 
		c.executemany('''
		INSERT OR REPLACE INTO settingsstreamers (streamername)
		VALUES (?)
		''', settingsDictionary['streamers'])

		# Populate table with streamteams
		c.executemany('''
		INSERT OR REPLACE INTO settingsteams (teamname, teamdisplayname)
		VALUES (?, ?)
		''', settingsDictionary['streamteams'])

		conn.commit()
		conn.close()

	def loadSettings(self):
		settingsDictionary = {}
		settingsDictionary['streamers'] = []
		settingsDictionary['streamteams'] = []

		conn = sqlite3.connect(self.DBFilePath)
		c = conn.cursor()

		for row in c.execute('SELECT * FROM settingsteams'):
			settingsDictionary['streamteams'].append(row)

		for row in c.execute('SELECT * FROM settingsstreamers'):
			settingsDictionary['streamers'].append(row)

		conn.commit()
		conn.close()

		return settingsDictionary