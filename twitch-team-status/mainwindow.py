from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from streamer import Streamer
from streamteam import StreamTeam
from tabpage import TabPage
from customcheckbox import CustomCheckBox
from settingswindow import SettingsWindow
import handlejson
import dbhandler
import settingshandler
import datetime
import os
import sys

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.teamsDictionary = {}
		self.tabPageDictionary = {}
		self.refreshThreads = []

		self.dbHandler = dbhandler.DBHandler(os.path.abspath(os.path.dirname(sys.argv[0])) + os.sep + "db" + os.sep + "database.sqlite3")
		self.checkedBoxes = self.dbHandler.fetchStreamerCheckboxes()

		self.settingsHandler = settingshandler.SettingsHandler(self.dbHandler)

		self.pendingChangesToSave = False
		self.wantedWindowTitle = 'Twitch Team Status'

		# Constants for the column location of certain things in the tablewidget grid
		self.HOSTED_CHECKBOX_COLUMN = 4
		self.VISITED_CHECKBOX_COLUMN = 5
		self.RAIDED_CHECKBOX_COLUMN = 6

		self.initUI()
		self.initMenuBar()

		self.settingsWindow = SettingsWindow(self.dbHandler, self.settingsHandler, self)
		

	# What happens when you try to .close() the top(main) Qt window
	# This applies to explicit calls to .close() and pressing the OS-side close button
	def closeEvent(self, event):
		if self.pendingChangesToSave:
			msgBox = QMessageBox()
			msgBox.setWindowTitle("Exit dialog")
			msgBox.setText("Do you want to save changes before quitting?")
			msgBox.setIcon(QMessageBox.Warning)
			noButton = msgBox.addButton(QMessageBox.No)
			cancelButton = msgBox.addButton(QMessageBox.Cancel)
			yesButton = msgBox.addButton(QMessageBox.Yes)
			msgBox.setDefaultButton(yesButton)

			dialogReply = msgBox.exec()

			clickedButton = msgBox.clickedButton()

			if clickedButton == yesButton:
				self.saveButtonAction()
				event.accept()

			elif clickedButton == noButton:
				event.accept()

			else:
				event.ignore()
		
	def initUI(self):
		grid = QGridLayout()
		grid.setSpacing(5)
		
		self.setGeometry(300, 300, 900, 600)
		self.setWindowTitle(self.wantedWindowTitle)

		self.tabs = QTabWidget()
		self.updateTabs()

		grid.addWidget(self.tabs, 0, 0, 5, 5)

		self.refreshButton = QPushButton("Refresh")
		self.refreshButton.setCursor(Qt.PointingHandCursor)
		self.refreshButton.clicked.connect(lambda: self.refreshButtonAction())

		self.refreshAllButton = QPushButton("Refresh All")
		self.refreshAllButton.setCursor(Qt.PointingHandCursor)
		self.refreshAllButton.clicked.connect(lambda: self.refreshAllButtonAction())

		quitButton = QPushButton("Quit")
		quitButton.setCursor(Qt.PointingHandCursor)
		quitButton.clicked.connect(lambda: self.quitButtonAction())

		saveButton = QPushButton("Save")
		saveButton.setCursor(Qt.PointingHandCursor)
		saveButton.clicked.connect(lambda: self.saveButtonAction())

		settingsButton = QPushButton("Settings")
		settingsButton.setCursor(Qt.PointingHandCursor)
		settingsButton.clicked.connect(lambda: self.settingsButtonAction())

		bottomPane = QWidget()
		bottomPaneL = QVBoxLayout()
		bottomPaneL.setAlignment(Qt.AlignVCenter)

		buttonpane = QWidget()
		buttonpaneL = QHBoxLayout()
		buttonpaneL.addStretch()

		buttonpaneL.addWidget(quitButton)
		buttonpaneL.addWidget(self.refreshButton)
		buttonpaneL.addWidget(self.refreshAllButton)
		buttonpaneL.addWidget(saveButton)
		buttonpaneL.addWidget(settingsButton)

		buttonpaneL.addStretch()
		buttonpane.setLayout(buttonpaneL)

		bottomPaneL.addWidget(buttonpane)
		bottomPane.setLayout(bottomPaneL)
		grid.addWidget(bottomPane, 5, 0, 1, 5)

		centralPart = QWidget()
		centralPart.setLayout(grid)
		self.setCentralWidget(centralPart)
		self.show()
		self.refreshAllButtonAction()
		self.statusBar().showMessage("Ready")

	def updateTabs(self):
		self.teamsInConfig = self.settingsHandler.getSettingsTeams()
		self.streamersInConfig = self.settingsHandler.getSettingsStreamers()

		amountOfTabs = self.tabs.count()

		for tab in range(0, amountOfTabs):
			self.tabs.removeTab(0)

		for streamTeam in self.teamsInConfig:
			self.tabPageDictionary[streamTeam.teamurl] = TabPage(streamTeam.teamurl, streamTeam.teamname)
			self.tabs.addTab(self.tabPageDictionary[streamTeam.teamurl], streamTeam.teamname)

		self.tabPageDictionary["users"] = TabPage("users", "Streamers")
		self.tabs.addTab(self.tabPageDictionary["users"], "Streamers")

	def initMenuBar(self):
		bar = self.menuBar()
		file = bar.addMenu("File")

		quitAction = file.addAction("Quit")
		quitAction.setShortcut('Ctrl+Q')

		refreshAction = file.addAction("Refresh")
		refreshAction.setShortcut('Ctrl+R')

		refreshAllAction = file.addAction("Refresh All")
		refreshAllAction.setShortcut('Ctrl+Shift+R')

		saveAction = file.addAction("Save")
		saveAction.setShortcut('Ctrl+S')

		file.triggered[QAction].connect(self.processtrigger)

	def processtrigger(self, action):
		act = action.text()
		
		if act == "Save":
			self.saveButtonAction()
		elif act == "Quit":
			self.quitButtonAction()
		elif act == "Refresh":
			self.refreshButtonAction()
		elif act == "Refresh All":
			self.refreshAllButtonAction()
		elif act == "Settings":
			self.settingsButtonAction()


	def quitButtonAction(self):
		self.close()

	def refreshPage(self, pageStreamTeam):
		if pageStreamTeam != "users":
			refreshThread = handlejson.JsonHandler(pageStreamTeam, "team")
		elif pageStreamTeam == "users":
			refreshThread = handlejson.JsonHandler(self.streamersInConfig, "users")
		else:
			print("wtf boiiiii")

		self.refreshThreads.append(refreshThread)
		refreshThread.signal.sig.connect(self.reactToReceivedStreamteam)
		refreshThread.start()

	def refreshButtonAction(self):
		self.currentPage = self.tabs.currentWidget()
		pageStreamTeam = self.currentPage.teamName
		self.refreshButton.setEnabled(False)
		self.statusBar().showMessage("Refreshing " + self.currentPage.teamDisplayName + "..")
		self.refreshPage(pageStreamTeam)


	def refreshAllButtonAction(self):
		self.refreshAllButton.setEnabled(False)
		self.statusBar().showMessage("Refreshing all teams..")

		for key, val in self.tabPageDictionary.items():
			self.refreshPage(key.lower())

	def saveButtonAction(self):
		self.dbHandler.save_streamer_checkboxes(self.checkedBoxes)
		self.dbHandler.delete_extra_rows()
		self.pendingChangesToSave = False
		self.refreshWindowTitle()
		self.statusBar().showMessage("Saved all changes")

	def settingsButtonAction(self):
		self.settingsWindow.show()

	# Reacts to streamteams emitted by for example JsonHandler
	def reactToReceivedStreamteam(self, streamteam):
		assert type(streamteam) is StreamTeam
		self.teamsDictionary[streamteam.teamname] = streamteam
		self.fillTable(streamteam) # Fill the Qt table
		self.refreshButton.setEnabled(True)
		self.refreshAllButton.setEnabled(True)
		self.statusBar().showMessage("Done")

	def fillTable(self, streamteam):
		assert type(streamteam) is StreamTeam

		futureRowList = []

		tableWidget = self.tabPageDictionary[streamteam.teamname.lower()].tableWidget
		tableWidget.clearContents()

		#Name column
		tableWidget.setColumnWidth(0,150)

		#Status column
		tableWidget.setColumnWidth(1,80)

		#Game column
		tableWidget.setColumnWidth(2,200)

		#Viewers
		tableWidget.setColumnWidth(3,70)

		#checkboxes
		tableWidget.setColumnWidth(4,60)
		tableWidget.setColumnWidth(5,60)
		tableWidget.setColumnWidth(6,60)

		tableWidget.setRowCount(len(streamteam.streamers))

		# Loop through dictionary of streamers in the team
		for key, streamer in streamteam.streamers.items():
			strname = streamer.name
			strstatus = streamer.status
			strgame = streamer.game
			strviewers = streamer.viewers

			strnameItem = QTableWidgetItem(strname)
			strstatusItem = QTableWidgetItem(strstatus)
			strgameItem = QTableWidgetItem(strgame)
			strviewersItem = QTableWidgetItem(strviewers)

			tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)


			# Style the table a little bit (The text colors depending on streamer status)
			if strstatus == "Online":
				strstatusItem.setForeground(QColor(46, 204, 113,255))
			else:
				wantedOpacity = 100
				strnameItem.setForeground(QColor(255, 255, 255, wantedOpacity))
				strstatusItem.setForeground(QColor(231, 76, 60, wantedOpacity))
				strgameItem.setForeground(QColor(255, 255, 255, wantedOpacity))
				strviewersItem.setForeground(QColor(255, 255, 255, wantedOpacity))

			# Pre-initialize the checkboxes
			hostedWidget = QWidget()
			visitedWidget = QWidget()
			raidedWidget = QWidget()

			hostedCheckBox = CustomCheckBox()
			visitedCheckBox = CustomCheckBox()
			raidedCheckBox = CustomCheckBox()

			if strname.lower() in self.checkedBoxes['hosted']:

				# Put the date next to checked checkbox
				date = str(self.checkedBoxes['hosted'][strname.lower()])
				if date != "None":
					hostedCheckBox.setText(date)
					hostedCheckBox.blockSignals(True)
					hostedCheckBox.setChecked(True)
					hostedCheckBox.blockSignals(False)

			if strname.lower() in self.checkedBoxes['visited']:
				date = str(self.checkedBoxes['visited'][strname.lower()])
				if date != "None":
					visitedCheckBox.setText(date)
					visitedCheckBox.blockSignals(True)
					visitedCheckBox.setChecked(True)
					visitedCheckBox.blockSignals(False)

			if strname.lower() in self.checkedBoxes['raided']:
				date = str(self.checkedBoxes['raided'][strname.lower()])
				if date != "None":
					raidedCheckBox.setText(date)
					raidedCheckBox.blockSignals(True)
					raidedCheckBox.setChecked(True)
					raidedCheckBox.blockSignals(False)

			hostedLayout = QHBoxLayout(hostedWidget)
			hostedLayout.addWidget(hostedCheckBox)
			hostedLayout.setAlignment(Qt.AlignCenter)
			hostedLayout.setContentsMargins(0,0,0,0)
			hostedWidget.setLayout(hostedLayout)

			visitedLayout = QHBoxLayout(visitedWidget)
			visitedLayout.addWidget(visitedCheckBox)
			visitedLayout.setAlignment(Qt.AlignCenter)
			visitedLayout.setContentsMargins(0,0,0,0)
			visitedWidget.setLayout(visitedLayout)

			raidedLayout = QHBoxLayout(raidedWidget)
			raidedLayout.addWidget(raidedCheckBox)
			raidedLayout.setAlignment(Qt.AlignCenter)
			raidedLayout.setContentsMargins(0,0,0,0)
			raidedWidget.setLayout(raidedLayout)

			futureRowList.append((strnameItem, strstatusItem, strgameItem, strviewersItem, hostedWidget, visitedWidget, raidedWidget))

		#Sort the table's items to appear with Online as primary attribute and viewers as second attribute
		futureRowList.sort(key=lambda tup: (tup[1].text(), int(tup[3].text())), reverse=True)

		for rowNr in range(0, len(futureRowList)):
			row = futureRowList[rowNr]
			tableWidget.setItem(rowNr, 0, row[0])
			tableWidget.setItem(rowNr, 1, row[1])
			tableWidget.setItem(rowNr, 2, row[2])
			tableWidget.setItem(rowNr, 3, row[3])
			tableWidget.setCellWidget(rowNr, 4, row[4])
			tableWidget.setCellWidget(rowNr, 5, row[5])
			tableWidget.setCellWidget(rowNr, 6, row[6])

		# Connecting every checkbox to their stateChanged signal
		# so we know when a checkbox has been checked/unchecked
		for x in range(0, tableWidget.rowCount()):
			hostedCheckBox = tableWidget.cellWidget(x, self.HOSTED_CHECKBOX_COLUMN).layout().itemAt(0).widget()
			hostedCheckBox.row = x
			hostedCheckBox.column = self.HOSTED_CHECKBOX_COLUMN
			hostedCheckBox.stateChanged.connect(self.checkboxChecked)

			visitedCheckBox = tableWidget.cellWidget(x, self.VISITED_CHECKBOX_COLUMN).layout().itemAt(0).widget()
			visitedCheckBox.row = x
			visitedCheckBox.column = self.VISITED_CHECKBOX_COLUMN
			visitedCheckBox.stateChanged.connect(self.checkboxChecked)

			raidedCheckBox = tableWidget.cellWidget(x, self.RAIDED_CHECKBOX_COLUMN).layout().itemAt(0).widget()
			raidedCheckBox.row = x
			raidedCheckBox.column = self.RAIDED_CHECKBOX_COLUMN
			raidedCheckBox.stateChanged.connect(self.checkboxChecked)

		self.tabPageDictionary[streamteam.teamname.lower()].reinitTableCols()


	def checkboxChecked(self, int):
		dateNow = datetime.datetime.now().date()
		self.currentPage = self.tabs.currentWidget()
		
		sender = self.sender()
		checkedRow = sender.row
		checkedColumn = sender.column

		wantedState = sender.isChecked()
		print("Wanted State: " + str(wantedState))
		personChecked = self.currentPage.tableWidget.item(checkedRow,0).text().lower()

		if checkedColumn == self.HOSTED_CHECKBOX_COLUMN:

			# If you're checking a checkbox
			if wantedState == True:
				self.checkedBoxes['hosted'][personChecked] = dateNow
				sender.setText(str(dateNow))

			# If you're unchecking a checkbox
			elif personChecked in self.checkedBoxes['hosted']:
				self.checkedBoxes['hosted'][personChecked] = None
				sender.setText("")

		elif checkedColumn == self.VISITED_CHECKBOX_COLUMN:

			# If you're checking a checkbox
			if wantedState == True:
				self.checkedBoxes['visited'][personChecked] = dateNow
				sender.setText(str(dateNow))

			# If you're unchecking a checkbox
			elif personChecked in self.checkedBoxes['visited']:
				self.checkedBoxes['visited'][personChecked] = None
				sender.setText("")

		elif checkedColumn == self.RAIDED_CHECKBOX_COLUMN:

			# If you're checking a checkbox
			if wantedState == True:
				self.checkedBoxes['raided'][personChecked] = dateNow
				sender.setText(str(dateNow))

			# If you're unchecking a checkbox
			elif personChecked in self.checkedBoxes['raided']:
				self.checkedBoxes['raided'][personChecked] = None
				sender.setText("")

		# Propagate the checkbox state to the streamer in all other streamteams
		for teamName, streamPage in self.tabPageDictionary.items():
			tableWidget = streamPage.tableWidget

			# Only go through the *other* stream teams' pages with populated tables
			if teamName != self.currentPage.teamName and tableWidget.rowCount() > 1:
				for streamerNr in range(0, tableWidget.rowCount()):
					if tableWidget.item(streamerNr,0).text().lower() == personChecked:
						checkboxToToggle = tableWidget.cellWidget(streamerNr, checkedColumn).layout().itemAt(0).widget()
						
						# Make sure not to cause a chain-reaction of signals by using blockSignals
						checkboxToToggle.blockSignals(True) 
						checkboxToToggle.setChecked(wantedState)
						checkboxToToggle.blockSignals(False)

			self.tabPageDictionary[teamName.lower()].reinitTableCols()

		self.pendingChangesToSave = True
		self.refreshWindowTitle()

	def refreshWindowTitle(self):

		# Add [Unsaved Changes] to window title if there are unsaved changes(duh)
		if self.pendingChangesToSave == True:
			self.setWindowTitle(self.wantedWindowTitle + " [Unsaved Changes]")
		else:
			self.setWindowTitle(self.wantedWindowTitle)
