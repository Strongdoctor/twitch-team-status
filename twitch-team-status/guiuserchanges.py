from abc import ABC, abstractmethod
# Represents any change to a streamer's properties in the QTableWidget

class AbstractChange(ABC):
	def __init__(self, affectedStreamer):
		self.affectedStreamer = affectedStreamer

class HostedChange(AbstractChange):
	def __init__(self, affectedStreamer):
		super().__init__(affectedStreamer)


change = HostedChange("ayy")
print(change.affectedStreamer)