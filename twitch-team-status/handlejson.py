from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from streamer import *
from streamteam import *
import json
import sys
import requests
import time
import decimal

debug = False
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
headers = {"Client-ID" : "c59xtelih7ubtlqam20bbalz3fy003",
			"Accept" : "application/vnd.twitchtv.v5+json"}

class StreamerSignal(QObject):
	sig = pyqtSignal(StreamTeam)

class JsonHandler(QThread):
	def __init__(self, streamteam, mode): #Mode is "team" or "users"
		QThread.__init__(self)
		self.streamteam = streamteam
		self.mode = mode
		self.signal = StreamerSignal()

	def run(self):
		teamusers = {}

		#Section for preparing the API-call links
		headers = {"Client-ID" : "c59xtelih7ubtlqam20bbalz3fy003",
           "Accept" : "application/vnd.twitchtv.v5+json"}
		teamurl = "https://api.twitch.tv/kraken/teams/" + str(self.streamteam)

		if self.mode == "team":
			users = getTeamUserJson(teamurl)

		elif self.mode == "users" and not isinstance(self.streamteam,str):
			users = getUsersJsonFromMiniUsersJson(getMiniUsersJsonFromNames(self.streamteam)["users"])

			self.streamteam = "users"
		else:
			print("Lmao fam wtf")



		# Create strimmers with default online status as "Offline"
		createBaseStreamers(users, teamusers)

		if debug:
			print("Create Streamers:" + str(decimal.Decimal(createStrimmerEndTime-createStrimmerStartTime)))

		streamrequest_objectifiedjson = getStreamsJson(teamusers)

		updateStatusStartTime = time.clock()
			
		# Update online status of all streamers
		for key, streamer in teamusers.items():
				for y in streamrequest_objectifiedjson['streams']:
					onlineuser = y
					if streamer.idnr == str(onlineuser['channel']['_id']):
						streamer.status = "Online"
						streamer.viewers = str(onlineuser['viewers'])

		updateStatusEndTime = time.clock()

		if debug:
			print("Update statuses: " + str(decimal.Decimal(updateStatusEndTime-updateStatusStartTime)))


		self.signal.sig.emit(StreamTeam(self.streamteam, teamusers))

def createBaseStreamers(users, teamusers):
	for x in range(0,len(users)):
		strimid = users[x]['_id']
		strimname = users[x]['display_name']
		strimgame = users[x]['game']
		strimmer = Streamer(strimid, strimname, "Offline", strimgame, "0")
		teamusers[strimname.lower()] = strimmer

def getTeamUserJson(teamurl):
	preRequestTime = time.clock()

	teamrequest = requests.get(teamurl,headers=headers)
	teamrequest_jsontext = teamrequest.text
	teamrequest_translatedjson = teamrequest_jsontext.translate(non_bmp_map)
	teamrequest_objectifiedjson = json.loads(teamrequest_translatedjson)
	users = teamrequest_objectifiedjson["users"];

	endRequestTime = time.clock()

	if debug:
		print("Request time:" + str(decimal.Decimal(endRequestTime-preRequestTime)))

	return users

def getUsersJsonFromMiniUsersJson(miniusers):
	users = []

	for x in range(0,len(miniusers)):
		tempuser = miniusers[x]
		userurl = "https://api.twitch.tv/kraken/channels/" + tempuser["_id"]
		userrequest_json = requests.get(userurl, headers=headers).text
		userrequest_translatedjson = userrequest_json.translate(non_bmp_map)
		userrequest_objectifiedjson = json.loads(userrequest_translatedjson)

		users.insert(x,userrequest_objectifiedjson)

	return users

def getStreamsJson(teamusers):
	streamsurl = "https://api.twitch.tv/kraken/streams/?channel="

	for k, v in teamusers.items():
		streamsurl += v.idnr + ","

	streamsurl[:-1] #Remove trailing comma from API request
	streamrequest = requests.get(streamsurl,headers=headers)
	streamrequest_jsontext = streamrequest.text
	streamrequest_translatedjson = streamrequest_jsontext.translate(non_bmp_map)
	streamrequest_objectifiedjson = json.loads(streamrequest_translatedjson)

	return streamrequest_objectifiedjson

def getMiniUsersJsonFromNames(userslist):

	usersurl = "https://api.twitch.tv/kraken/users?login="

	for x in userslist:
		usersurl += x.streamername + ","

	usersurl = usersurl[:-1] #Remove trailing comma from API request
	usersrequest = requests.get(usersurl, headers=headers)
	usersrequest_jsontext = usersrequest.text
	usersrequest_translatedjson = usersrequest_jsontext.translate(non_bmp_map)
	usersrequest_objectifiedjson = json.loads(usersrequest_translatedjson)

	return usersrequest_objectifiedjson